package cmd

import (
	"context"
	"errors"
	"strings"

	api "gitlab.com/bloom-flare/string_service/api"
)

// Uppercase function returns a copy of the string with all Unicode letters mapped to their upper case.
func (s *StringServiceServer) Uppercase(ctx context.Context, request *api.UppercaseRequest) (*api.UppercaseResponse, error) {
	if request.Input == "" {
		return &api.UppercaseResponse{}, errors.New("Empty string")
	}
	return &api.UppercaseResponse{
		Message: strings.ToUpper(request.Input),
	}, nil
}
