package main

import (
	"fmt"
	"net"
	"os"
	"time"

	glg "github.com/kpango/glg"
	cli "github.com/urfave/cli"
	api "gitlab.com/bloom-flare/string_service/api"
	cmd "gitlab.com/bloom-flare/string_service/cmd"
	grpc "google.golang.org/grpc"
	credentials "google.golang.org/grpc/credentials"
)

const (
	appVersion string = "0.0.1"
)

var infolog = glg.FileWriter(fmt.Sprintf("/log/info.%s.log",
	time.Now().Format("2006_01_02_1504")), 0666)

func init() {
	defer glg.Get().
		SetMode(glg.BOTH).
		AddLevelWriter(glg.INFO, infolog).
		AddLevelWriter(glg.ERR, infolog).
		AddLevelWriter(glg.FAIL, infolog).
		AddLevelWriter(glg.FATAL, infolog).
		Stop()
}

func main() {
	defer infolog.Close()
	app := cli.NewApp()
	app.Name = "String Service"
	app.Description = "String Service provides operations on strings over gRPC"
	app.Version = appVersion
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "port, p",
			Value:  "8440",
			Usage:  "port for the tcp server",
			EnvVar: "STRINGSVC_TCP_PORT",
		},
		cli.StringFlag{
			Name:   "cert, c",
			Value:  "server-cert.pem",
			Usage:  "TLS credentials file",
			EnvVar: "SMTPSVC_CERT",
		},
		cli.StringFlag{
			Name:   "key, k",
			Value:  "server-key.pem",
			Usage:  "TLS key file",
			EnvVar: "SMTPSVC_KEY",
		},
	}

	app.Action = func(c *cli.Context) error {
		// Create a listener on given TCP port
		lis, err := net.Listen("tcp", fmt.Sprintf(":%s", c.String("port")))
		if err != nil {
			return err
		}

		// Load all the the credentials from file
		creds, err := credentials.NewServerTLSFromFile(c.String("cert"), c.String("key"))
		if err != nil {
			return err
		}

		// Creates a new gRPC server with UnaryInterceptor,
		// and with restricted message size (20KB)
		server := grpc.NewServer(
			grpc.Creds(creds),
			grpc.MaxMsgSize(20*1024),
			withServerUnaryInterceptor(),
		)

		// Attach the String Service to the server
		api.RegisterStringServiceServer(server, &cmd.StringServiceServer{})

		// Start the server
		err = server.Serve(lis)
		if err != nil {
			return err
		}

		return nil
	}

	err := app.Run(os.Args)
	if err != nil {
		glg.Fatal(err)
	}
}

func withServerUnaryInterceptor() grpc.ServerOption {
	return grpc.UnaryInterceptor(loggerInterceptor)
}
